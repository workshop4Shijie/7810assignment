import unittest
import word_Ladder

class Test_same(unittest.TestCase):
    def test_same_1(self):
        result = word_Ladder.same("lead", "gold")
        expect = 1
        self.assertEqual(result, expect)

    def test_same_2(self):
        result = word_Ladder.same("dive", "cile")
        expect = 2
        self.assertEqual(result, expect)

    def test_same_3(self):
        result = word_Ladder.same("seek", "leek")
        expect = 3
        self.assertEqual(result, expect)

    def test_same_4(self):
        result = word_Ladder.same("ward", "ward")
        expect = 4
        self.assertEqual(result, expect)

    def test_same_0(self):
        result = word_Ladder.same("lead", "cole")
        expect = 0
        self.assertEqual(result, expect)

class Test_build(unittest.TestCase):
    def test_build_1(self):
        words = ["lead", "gold", "good", "cole"]
        for word in words:
            for i in range(len(word)):
                result = word_Ladder.build(word[:i] + "." + word[i + 1:], words, {"type" : True}, [(0, "cape")])
        expect = ["cole"]
        self.assertEqual(result, expect)

    def test_build_2(self):
        pattern = ".ood"
        words = ["good", "lead", "load", "cole", "true", "self"]
        seen = {"come": True}
        list = ["test","gold"]
        result = word_Ladder.build(pattern, words, seen, list)
        expect = ["good"]
        self.assertEqual(result, expect)

    def test_build_3(self):
        pattern = ".ood"
        words = ["good", "lead", "load", "cole", "true", "self"]
        seen = {"good": True}
        list = ["test","gold"]
        result = word_Ladder.build(pattern, words, seen, list)
        expect = []
        self.assertEqual(result, expect)

    def test_build_4(self):
        pattern = "l.af"
        words = ["good", "lead", "load", "cole", "true", "self", "leaf"]
        seen = {"come": True}
        list = ["test","gold"]
        result = word_Ladder.build(pattern, words, seen, list)
        expect = ["leaf"]
        self.assertEqual(result, expect)

    def test_build_5(self):
        pattern = "see."
        words = ["good", "lead", "load", "cole", "true", "self", "seek"]
        seen = {"come": True}
        list = ["test","gold"]
        result = word_Ladder.build(pattern, words, seen, list)
        expect = ["seek"]
        self.assertEqual(result, expect)

class Test_Dictionary_File(unittest.TestCase):
    def test_Dictionary_1(self):
        result = word_Ladder.checkOpenDictionary('')
        expect = False
        self.assertAlmostEqual(result,expect)

    def test_Dictionary_2(self):
        result = word_Ladder.checkOpenDictionary("text.txt")
        expect = True
        self.assertAlmostEqual(result,expect)

    def test_Dictionary_3(self):
        result = word_Ladder.checkOpenDictionary("ksjklsjflsjlksjflks")
        expect = False
        self.assertAlmostEqual(result,expect)

class Test_Valid_word(unittest.TestCase):
    def test_valid_word_1(self):
        result = word_Ladder.checkWordValid("", ["test", "word", "lead"])
        expect = False
        self.assertEqual(result, expect)

    def test_valid_word_2(self):
        result = word_Ladder.checkWordValid("lead", ["test", "word", "lead"])
        expect = True
        self.assertEqual(result, expect)

    def test_valid_word_3(self):
        result = word_Ladder.checkWordValid("ksjlsjksjls", ["test", "word", "lead"])
        expect = False
        self.assertEqual(result, expect)


class Test_find(unittest.TestCase):
    def test_findShort(self):
        words=["hide", "good", "lead", "gold", "hide", "cole"]
        result = word_Ladder.findShort("hide", words, {"hide": True}, "seek", [(0, "hide")])
        expect = False
        self.assertEqual(result, expect)

    def test_findMax(self):
        words = ["hide","lead","load", "gold", "good", "goad"]
        path = ["lead", "load", "goad", "gold"]
        result = word_Ladder.findMax("lead", words, {"lead" : True}, "gold", path)
        expect = True
        self.assertAlmostEqual(result, expect)

if __name__ == '__main__':
    unittest.main()
