import re
import os
import os.path

#try to find out the number of the same letters between start word and target word
#using zip() function, scanning each letter of start and target word
#it will return a list of the same letter among start and target
#and count the number of the same list
def same(item, target):
  return len([c for (c, t) in zip(item, target) if c == t])

#try to find every matched same number of words
#AND have not seen before AND not in the list that been created
# this is the recursion function
def build(pattern, words, seen, list):
  return [word for word in words
                 if re.search(pattern, word) and word not in seen.keys() and
                    word not in list]

#this is the existing find() word path and has the maxium steps
#For clear expression, I changed it to findMax(), because further functions are writing
def findMax(word, words, seen, target, path):
  list = []
  for i in range(len(word)):
    list += build(word[:i] + "." + word[i + 1:], words, seen, list)
  if len(list) == 0:
    return False
  list = sorted([(same(w, target), w) for w in list])
  for (match, item) in list:
    if match >= len(target) - 1:
      if match == len(target) - 1:
        path.append(item)
      return True
    seen[item] = True
  for (match, item) in list:
    path.append(item)
    if findMax(item, words, seen, target, path):
      return True
    path.pop()

#the function to find out the shortest length
def findShort(word, words, seen, target, path):
    list = []
    for i in range(len(word)):  #choose the same amount letters of words in the dictionary and form them to the given list [] above
        list += build(word[:i] + "." + word[i + 1:], words, seen, list)
    if len(list) == 0:
        return False
    list = sorted([(same(v, target), v) for v in list], reverse=True)#we need to return a list including words has the same letters compare with the target
    for (match, item) in list:                   #judge the matched letters, if the matched letters same as target
        if match >= len(target) - 1:
            if match == len(target) - 1:
                path.append((match, item))
            return True
        seen[item] = True
    for (match, item) in list:
        if match >= path[-1][0]:            #compare the number in list with the number of the same letter in known path
            path.append((match, item))
            if findShort(item, words, seen, target, path):
                return True
            path.pop()
        else:
            continue

def findMedium(word, words, seen, target, path):
    list = []
    for i in range(len(word)):  #choose the same amount letters of words in the dictionary and form them to the given list [] above
        list += build(word[:i] + "." + word[i + 1:], words, seen, list)
    if len(list) == 0:
        return False
    list = sorted([(same(v, target), v) for v in list])#we need to return a list including words has the same letters compare with the target
    print(list, "sorted")
    for (match, item) in list:                   #judge the matched letters, if the matched letters same as target
        if match >= len(target) - 1:
            if match == len(target) - 1:
                path.append((match, item))
                print(path,"append path 1st for loop")
            return True
        seen[item] = True

    for (match, item) in list:
        if match >= path[-1][0]:            #compare the number in list with the number of the same letter in known path
            path.append((match, item))
            print(path, "2nd for loop")
            if findShort(item, words, seen, target, path):
                return True
            path.pop()
        else:
            continue

#the function checking the input dictionary is valid, using the os library, path method
def checkOpenDictionary(filename):
    if os.path.isfile(filename):
        return True
    else:
        return False

#check the input words are valid based on the input dictionary
def checkWordValid(checkOne, words):
    if checkOne in words:
        return True
    else:
        return False


#########################################################
#Following is the MAIN function to run the program above#

#check the input dictionary is valid
while True:
    fname = input("Enter dictionary name: ")
    if checkOpenDictionary(fname):
        break
    else:
        print("This is not a valid dictionary file or not exist, ")

file = open(fname)
lines = file.readlines()             #open a dictionary and read lines

#input the start word and check it valid or not
while True:
    start = input("Enter start word:").lower().strip()
    words = []
    for line in lines:                 #get rid of the blanks at the right hand side of each line
        word = line.rstrip()
        if len(word) == len(start):      #compare the length of start and target word, form the same length of words into words
            words.append(word)
    if checkWordValid(start, words):
         break
    else:
        print("The start word is not valid")

#enter the target word and check it is a valid word or not
while True:
    target = input("Enter target word:").lower().strip()
    if checkWordValid(target, words):
         break
    else:
        print("The target word is not valid")

#variables among this file, will be used in the find functions
path = [(0, start)]    #make it a list of tuple and initial match value is 0
seen = {start : True}

#ask for the not allowed words,if answer yes, then user should input the words split by space
no_allowed_word = []
ask_for_not_allow = input("Do you have words not allowed for input? Y/N " ).lower().strip()
if ask_for_not_allow == "y":
    no_allowed_word = [word for word in input("Please write(split by space): ").split()]
if len(no_allowed_word)>0:
    for item in no_allowed_word:
        seen[item] = True

#ask for the length, shortest or longest or certain length
LengthOfStep=input("Do you want a shortest path or longest or give me a certain length? (Select 1,2 or 3) (1)Shortest (2)Longest (3)Certen_Length: ")
if LengthOfStep == "1":
    if findShort(start, words, seen, target, path):
        list11 = []
        path.append((len(target), target))   #match with the path(number, word) pattern
        for w, v in path:
            list11.append(v)
        print(len(path) - 1, list11)
    else:
        print("no path found")
elif LengthOfStep == "2":
    if findMax(start, words, seen, target, path):
        path.append(target)
        print(len(path) - 1, path)
    else:
        print("no path found")
if LengthOfStep == "3":
    #user selected a certain length to here,
    the_certain_length = int
    while True:
        try:
            the_certain_length = int(input("Please enter a number for a certain length: "))
            print("Not a possible length, loading the closest length of path")
            if findMedium(start, words, seen, target, path):
                list11 = []
                path.append((len(target), target))  # match with the path(number, word) pattern
                for w, v in path:
                    list11.append(v)
                print(len(path) - 1, list11)
            else:
                print("no path found")
            break
        except ValueError:
            print("Not a valid number")

